"use strict";
document.addEventListener('DOMContentLoaded',setup);
let arr = [];

function setup(){
    //let btn = document.querySelector('button');
    let form = document.querySelector('form');
    //btn.addEventListener('submit',submit);
    form.addEventListener('submit',submit); //using button for eventlistener for submit doesnt work
    loadTodo();
}

function loadTodo(){
    if(localStorage.getItem('alltasks') !== null){
        arr = JSON.parse(localStorage.getItem('alltasks'));
        for(const task of arr){
            appendNewTodo(task);
        }
    }
}
function checkedAction(e){
    let target = e.target.parentNode.parentNode.parentNode.parentNode;
    arr.splice(parseInt(target.id),1);
    document.querySelector('#tasksection').removeChild(target);
    updateUniqueId();
    updatelocalStorage(arr);
}
//uniqueid same as array index
function updateUniqueId(){
    let tasks = document.querySelectorAll('article');
    for(let j = 0; j < tasks.length;j++){
        arr[j].uniqueid = j;
        tasks[j].id = j;
    }
}
function submit(e){
    e.preventDefault();
     let newTodo = {
        uniqueid: arr.length,
        task: document.querySelector('#task').value,
        desc: document.querySelector('#desc').value,
        importance: document.querySelector('#importance').value,
        color: getColor(document.querySelectorAll('.category'))
    }
    arr.push(newTodo);
    updatelocalStorage(arr);
    appendNewTodo(newTodo);
}
function updatelocalStorage(arr){
    let allTasks = JSON.stringify(arr);
    window.localStorage.setItem('alltasks',allTasks);
}
function appendNewTodo(newTodo){
    let taskArticle = createArticle(newTodo.uniqueid);
    let divColor = createColorDiv(newTodo.color);
    let main = createMain();
    let header = createHeader();
    let taskname = createTaskName(newTodo.task);
    let desc = createDescription(newTodo.desc);
    let stars = createStars(newTodo.importance);
    stars.innerHTML = getStars(newTodo.importance);
    let checkbox = createCheckBox();
    let divCheckStars = createDivCheckStars();

    divCheckStars.appendChild(stars);
    divCheckStars.appendChild(checkbox);
    header.appendChild(taskname);
    header.appendChild(divCheckStars);
    taskArticle.appendChild(divColor);
    main.appendChild(header);
    main.appendChild(desc);
    taskArticle.appendChild(main);

    document.body.querySelector('#tasksection').appendChild(taskArticle);
}
function createArticle(uniqueid){
    let taskArticle = document.createElement('article');
    taskArticle.style.backgroundColor = 'white';
    taskArticle.style.borderRadius = '2em';
    taskArticle.style.marginTop = '0.5em';
    taskArticle.style.width = '30em';
    taskArticle.style.height = '7em';
    taskArticle.style.padding = '1em';
    taskArticle.style.display = 'flex';
    taskArticle.style.flexflow = 'row';
    taskArticle.id = uniqueid;

    return taskArticle;
}
function getCheckedCategory(category){
    let checkedCategory;
    category.forEach(
        function(categoryType){
            if(categoryType.checked){
                checkedCategory = categoryType;
            }
    }
    );
    return checkedCategory;
}
function getColor(category){
    const checkedCategory = getCheckedCategory(category);
    switch (checkedCategory.value){
        case 'school':
            return 'green';
        case 'work':
            return 'orange';
        case 'personal':
            return 'purple';
        default:
            console.log('Error getting category color'); 
    }
}
function createColorDiv(color){
    let div = document.createElement('div');
    div.style.width = '1.5em';
    div.style.height = '100%';
    div.style.marginRight = '1em';
    div.style.backgroundColor = color;

    return div;
}
function createMain(){
    let main = document.createElement('main');
    main.style.display = 'flex';
    main.style.flexFlow = 'column';
    main.style.justifyContent = 'space-around';
    main.style.flex = '100%';
    return main;
}
function getStars(importance){
    switch (importance) {
        case '1':
            return '&#x2605; &#x2606; &#x2606;';
        case '2':
            return '&#x2605; &#x2605; &#x2606;';
        case '3':
            return '&#x2605; &#x2605; &#x2605;';
        default:
            return '&#x2606; &#x2606; &#x2606;';
    } 
}
function createCheckBox(){
    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.style.transform = 'scale(1.2)';
    checkbox.addEventListener('click',checkedAction);
    
    return checkbox;
}
function createHeader(){
    let header = document.createElement('header');
    header.style.display = 'flex';
    header.style.flexFlow = 'row';
    header.style.justifyContent = 'space-between';
    header.style.alignItems = 'center';
    return header;
}
function createDivCheckStars(){
    let div = document.createElement('div');
    div.style.display = 'flex';
    div.style.flexFlow = 'row';
    div.style.justifyContent = 'space-around';
    div.style.flex = '1';
    div.style.alignItems = 'center';

    return div;
}
function createTaskName(task){
    let taskname = document.createElement('h2');
    taskname.textContent = task;
    taskname.style.overflowWrap = 'anywhere';
    taskname.style.flex = '3';

    return taskname;
}
function createDescription(description){
    let desc = document.createElement('p');
    desc.textContent = description;
    desc.style.overflowWrap = 'anywhere';

    return desc;
}
function createStars(importance){
    let stars = document.createElement('p');
    stars.style.fontSize = '20px';
    stars.style.color = 'blue';
    stars.innerHTML = getStars(importance);

    return stars;
}

